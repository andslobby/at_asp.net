﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Calculator.Tests
{
    [TestClass]
    public class CalculatorOpTest
    {
        [TestMethod]
        public void Sum_9Plus5_returns14()
        {
            CalculatorOp obj = new CalculatorOp();
            int a = 9;
            int b = 5;
            int expected = 14;

            int actual = obj.Add(a, b);

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Op_5mult6_returns30()
        {
            CalculatorOp obj = new CalculatorOp();
            int a = 5;
            int b = 6;
            int expected = 30;

            int actual = obj.Mult(a, b);

            Assert.AreEqual(expected, actual);
        }
    }
}
