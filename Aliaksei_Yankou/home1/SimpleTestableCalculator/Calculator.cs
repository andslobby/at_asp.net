﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleTestableCalculator
{
	public class Calculator
	{
		public int Add(int augend, int addend)
		{
			return checked(augend + addend);
		}

		public int Subtract(int minuend, int subtrahend)
		{
			return checked(minuend - subtrahend);
		}

		public int Multiply(int multiplicand, int multiplier)
		{
			return checked(multiplicand*multiplier);
		}

		public int Divide(int dividend, int divisor)
		{
			return (dividend/divisor);
		}
	}
}
