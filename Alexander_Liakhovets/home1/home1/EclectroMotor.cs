﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace home1
{
	public class EclectroMotor : IMotor
	{
		private int currentSpeed;
		private DirectionOfRotation directionofRotation;


		public EclectroMotor()
		{
			currentSpeed = 0;
			directionofRotation = DirectionOfRotation.None;
		}

		public DirectionOfRotation CurrentDirection
		{
			get
			{
				return directionofRotation;
			}
		}

		public int Speed
		{
			get
			{
				return currentSpeed;
			}
		}

		public void SpeedUp(int speedIncrement)
		{
			currentSpeed = currentSpeed + speedIncrement;
		}

		public void Start(DirectionOfRotation direction)
		{
			directionofRotation = direction;
			currentSpeed = 1;
		}

		public void Stop()
		{
			directionofRotation = DirectionOfRotation.None;
			currentSpeed = 0;
		}
	}
}
