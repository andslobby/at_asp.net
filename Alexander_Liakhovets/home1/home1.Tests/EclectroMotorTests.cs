﻿using NUnit.Framework;

namespace home1.Tests
{
	[TestFixture]
	public class EclectroMotorTests
	{
		IMotor motor;

		[SetUp]
		public void SetUp()
		{
			motor = new EclectroMotor();
		}

		[TearDown]
		public void TearDown()
		{
			motor = null;
		}

		[TestCase(10)]
		[TestCase(22)]
		[TestCase(0)]
		public void SpeedUpTest(int increment)
		{
			int oldSpeed = motor.Speed;
			motor.SpeedUp(increment);
			int newSpeed = motor.Speed;

			Assert.AreEqual(oldSpeed + increment, newSpeed);
		}

		[Test]
		public void StartTest()
		{
			motor.Start(DirectionOfRotation.Right);

			Assert.That(motor.Speed, Is.Not.EqualTo(0));
			Assert.AreEqual(DirectionOfRotation.Right, motor.CurrentDirection);
		}

		[Test]
		public void StopTest()
		{
			motor.Stop();

			Assert.AreEqual(motor.Speed, 0);
			Assert.AreEqual(DirectionOfRotation.None, motor.CurrentDirection);
		}
	}
}
