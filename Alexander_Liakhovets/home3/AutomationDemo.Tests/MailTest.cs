﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutomationDemo.Pages;
using AutomationDemo.Services;
using AutomationDemo.Services.Models;
using NUnit.Framework;

namespace AutomationDemo.Tests
{
	class MailTest : BaseTest
	{
		LoginService loginService = new LoginService();
		BaseMailPage baseMailPage = new BaseMailPage();

		[Test]
		public void MailTest_TryToSendTestMessage_ActualDoneMessage()
		{
			loginService.LoginToMailBox(AccountFactory.Account);
			bool result = baseMailPage
				.CreateNewMail()
				.SendTestMessage(AccountFactory.Account.Email)
				.IsDoneMessage();

			Assert.That(result);
		}

		[Test]
		public void MailTest_SenMailWithEmptyToInput_CorrectErrorMessage()
		{
			NewMailPage newMailPage = new NewMailPage();

			loginService.LoginToMailBox(AccountFactory.Account);
			baseMailPage.CreateNewMail().SendTestMessage(string.Empty);
			bool result = newMailPage.IsErrorMessage();

			Assert.That(result);
		}

		[Test]
		public void MailTest_SendTestMessage_TestMessageInInboxList()
		{
			loginService.LoginToMailBox(AccountFactory.Account);
			bool result = baseMailPage
				.CreateNewMail()
				.SendTestMessage(AccountFactory.Account.Email)
				.GoToInboxPage()
				.FindTestMessge();

			Assert.That(result);
		}

		[Test]
		public void MailTest_SendTestMessage_TestMessageInOutboxList()
		{
			loginService.LoginToMailBox(AccountFactory.Account);
			bool result = baseMailPage
			.CreateNewMail()
			.SendTestMessage(AccountFactory.Account.Email)
			.GoToSentPage()
			.FindTestMessge();

			Assert.That(result);
		}

		[Test]
		public void MailTest_DeleteTestMessage_TestMessageInTrash()
		{
			loginService.LoginToMailBox(AccountFactory.Account);
			bool result = baseMailPage
				.CreateNewMail()
				.SendTestMessage(AccountFactory.Account.Email)
				.GoToInboxPage()
				.DeleteTestMessage()
				.GotoTrashPage()
				.FindTestMessge();

			Assert.That(result);
		}

		[Test]
		public void MailTest_PermanentDeleteTestMessage_EmptyTrash()
		{
			loginService.LoginToMailBox(AccountFactory.Account);
			bool result = baseMailPage
				.CreateNewMail()
				.SendTestMessage(AccountFactory.Account.Email)
				.GoToInboxPage()
				.DeleteTestMessage()
				.GotoTrashPage()
				.SelectAllMails()
				.PermanentDeleteTestMessage()
				.FindTestMessge();

			Assert.That(!result);
		}
	}
}
