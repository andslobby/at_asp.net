﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Calculator
{
    public class Calculator
    {
        public int Sum(int a, int b)
        {
            return (a + b);
        }

        public int Diff(int a, int b)
        {
            return (a - b);
        }

        public double Div(int a, int b)
        {
            try
            {
                return ((double)a / b);
            }
            catch(DivideByZeroException)
            {
                throw new DivideByZeroException(message: "Division by zero is impossible.");
            }
        }

        public int Multiply(int a, int b)
        {
            return (a * b);
        }
        
    }
}
