﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Calculator
{
    class Program
    {
        static void Main(string[] args)
        {
            AnyNumber anyNumber = new AnyNumber();
            int a = anyNumber.GetAnyNumber();
            int b = anyNumber.GetAnyNumber();
            Calculator calc=new Calculator();
            Console.WriteLine("{0}+{1}={2}",a, b, calc.Sum(a, b));
            Console.WriteLine("{0}-{1}={2}", a, b, calc.Diff(a, b));
            Console.WriteLine("{0}*{1}={2}", a, b, calc.Multiply(a, b));
            Console.WriteLine("{0}/{1}={2}", a, b, calc.Div(a, b));
            Console.ReadKey();

        }
    }
}
