﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using MyCalcLib;
using Moq;

namespace MyCalcLibTests
{
    [TestFixture]
    class SequenceMyCalcTests
    {
        //Using moq
        [Test]
        public void nextSumTest_3plus4minus2plus1_returns6()
        {
            var mock = new Mock<ISequence>();
            mock.Setup(sequence => sequence.NextInts()).Returns(new List<int>() { 3, 4, -2, 1 });

            SequenceMyCalc sequenceMyCalc = new SequenceMyCalc(mock.Object);
            int expected = 6;

            int actual = sequenceMyCalc.NextSum();

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void nextSumTest_8plus7_returns15()
        {
            // arrange
            var sequence = new MockSequence();
            sequence.Values = new List<int>() { 8, 7 };
            SequenceMyCalc sequenceMyCalc = new SequenceMyCalc(sequence);
            int expected = 15;

            // act
            int actual = sequenceMyCalc.NextSum();

            // assert
            Assert.AreEqual(expected, actual);
        }
    }
}
