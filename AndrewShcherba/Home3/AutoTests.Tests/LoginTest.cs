﻿using NUnit.Framework;
using AutoTests.Models;
using AutoTests.Services;
using System;


namespace AutoTests.Tests
{
    [TestFixture]
    class LoginTest : BaseTest
    {
        LoginService loginService = new LoginService();
        private const string expected_error_message_in_case_of_wrong_account = "Нет аккаунта с таким логином.";
        private const string expected_error_message_in_case_of_wrong_password = "Неправильный логин или пароль.";

        [Test]
        public void LoginTest_LoginWithCorrectAccount()
        {
            loginService.LoginToMailBox(AccountFactory.Account);
            Assert.That<String>(AccountFactory.Account.Mail, Is.EqualTo(loginService.ChekMailAcount()));
        }


        [Test]
        public void LoginTest_LoginWithWrongAccount_CorrectErrorMessagePresent()
        {
            loginService.LoginToMailBox(AccountFactory.AccountWithWrongLogin);
            Assert.That(expected_error_message_in_case_of_wrong_account, Is.EqualTo(loginService.RetrieveErrorOnFailedLogin()));
        }

        [Test]
        public void LoginTest_LoginWithWrongPaswd_CorrectErrorMessagePresent()
        {
            loginService.LoginToMailBox(AccountFactory.AccountWithWrongPassword);
            Assert.That(expected_error_message_in_case_of_wrong_password, Is.EqualTo(loginService.RetrieveErrorOnFailedLogin()));
        }
    }
}
