﻿using AutoTests.Utilities;
using OpenQA.Selenium;

namespace AutoTests.Pages
{
    class MailMainPage
    {
        public IWebElement LoginMail { get { return Browser.FindElement(By.CssSelector("div.mail-User-Name")); } }
        public IWebElement WriteMailHref { get { return Browser.FindElement(By.CssSelector(".ns-view-container-desc a[href='#compose']")); } }
        public IWebElement FieldTo { get { return Browser.FindElement(By.CssSelector("div[name='to']")); } }
        public IWebElement FieldSubject { get { return Browser.FindElement(By.CssSelector("input[name=subj]")); } }
        public IWebElement FieldBody { get { return Browser.FindElement(By.CssSelector("div[role='textbox']")); } }
        public IWebElement FieldBody1 { get { return Browser.FindElement(By.CssSelector("div[role='textbox'] div")); } }
        public IWebElement SendMailButton { get { return Browser.FindElement(By.CssSelector(".js-send-button")); } }

    }
}
