﻿namespace AutoTests.Models
{
    public class UserAccount
    {
        public string Login { get; }
        public string Password { get; }
        public string Mail { get; }

        public UserAccount(string name, string password, string mail)
        {
            Login = name;
            Password = password;
            Mail = mail;
        }
    }
}
