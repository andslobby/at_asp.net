﻿using System;
using OpenQA.Selenium;
using AutoTests.Pages;
using System.Threading;
using OpenQA.Selenium.Interactions;
using AutoTests.Utilities;

namespace AutoTests.Services
{
    public class MailService
    {
        private MailMainPage MailPage = new MailMainPage();
        private SentMailPage SentPage = new SentMailPage();
        private InBoxMailPage InBoxPage = new InBoxMailPage();
        private TrashMailPage TrashPage = new TrashMailPage();
        string to;
        string subj;

        public string Subj
        {
            get { return subj; }
        }

        public void SendMail(string to, string body)
        {
            this.to = to;
            MailPage.WriteMailHref.Click();
            MailPage.FieldTo.SendKeys(to);
            subj = DateTime.Now.ToString();
            MailPage.FieldSubject.SendKeys(subj);
            MailPage.FieldBody.SendKeys(body);
            Thread.Sleep(10000);             //js-shit happens
            MailPage.SendMailButton.Click();

        }

        public string CheckSentMail()
        {
            SentPage.SentButton.Click();
            return SentPage.FindLetter(to, subj).Text;
        }

        public string CheckInBoxMail()
        {
            InBoxPage.InboxButton.Click();
            return InBoxPage.FindLetter(to, subj).Text;
        }

        public void DelFormSentLetter()
        {
            SentPage.SentButton.Click();
            SentPage.FindCheckBoxLetter(to, subj).Click();
            SentPage.DeleteButton.Click();
        }

        public string CheckDeletedMail()
        {
            Thread.Sleep(500);          //shit happens
            TrashPage.TrashButton.Click();
            return TrashPage.FindLetter(to, subj).Text;
        }

        public void DelFormTrashLetter()
        {
            Thread.Sleep(500);          //shit happens
            TrashPage.TrashButton.Click();
            TrashPage.FindCheckBoxLetter(to, subj).Click();
            TrashPage.DeleteButton.Click();
        }

        public bool CheckIsPermanentDeletedMail()
        {
            string textSubj;
            Thread.Sleep(500);                  //js-shit happens
       
            try
            {
                textSubj = TrashPage.FindLetter(to, subj).Text;
            }
            catch(NoSuchElementException)
            {
                return true;       
            }
            return false;
        }

    }
}
