﻿На странице google.com выписать следующие CSS и XPATH селекторы:
1. Строка для ввода кейворда для поиска
//*[@id='lst-ib']
//input[@id='lst-ib']

2. Кнопка "Поиск в Google"
//input[@name='btnK']
//*[@name='btnK']

3. Кнопка "Мне повезет"
//input[@name='btnI']
//*[@name='btnI']

На странице результатов гугла:
1. Ссылки на результаты поиска (все)
//a/ancestor::h3[@class='r']

2. 5-я буква о в Goooooooooogle (внизу, где пагинация)
.//*[@id='nav']//a[@aria-label='Page 5']/span
//table[@id='nav']/tbody/tr/td[6]/a/span
//tbody/tr/td[6]/a/span
//td[6]/a/span
.//*[@id='nav']/tbody/tr/td[6]/a/span

На странице yandex.com:
1. Login input field
//input[@name='login']

2. Password input field
//input[@name='passwd']

3. "Enter" button in login form
//form[contains(@class, 'domik2')]//button[@type='submit']
//button[@type='submit']

На странице яндекс почты (у кого нет ящика - зарегистрируйте)
1. Ссылка "Входящие"
//*[@id='nb-1']//a[descendant::span[text()='Входящие']]
//a[descendant::a[contains(@href, "#inbox")]]

2. Ссылка "Исходящие"
//*[@id='nb-1']//a[descendant::span[text()='Отправленные']]
//*[@id='nb-1']//a[contains(@href, '#sent')]
//a[contains(@href, '#sent')]

3. Ссылка "Спам"
//*[@id='nb-1']//a[descendant::span[text()='Спам']]
//*[@id='nb-1']//a[contains(@href, '#spam')]
//a[contains(@href, '#spam')]

4. Ссылка "Удаленные"
//*[@id='nb-1']//a[descendant::span[text()='Удалённые']]
//*[@id='nb-1']//a[contains(@href, '#trash')]
//a[contains(@href, '#trash')]

5. Ссылка "Черновики"
//*[@id='nb-1']//a[descendant::span[text()='Черновики']]
//*[@id='nb-1']//a[contains(@href, '#draft')]
//a[contains(@href, '#draft')]

6. Кнопка "Новое письмо"
//*[@id='nb-1']//a[descendant::span[text()='Написать']]
//*[@id='nb-1']//a[contains(@href, '#compose')]
//a[contains(@href, '#compose')]

7. Кнопка "Обновить"
//div[contains(@data-click-action, "mailbox.check")]

8. Кнопка "Отправить" (на странице нового письма)
//button[@id='nb-32']

9. Кнопка "Пометить как спам"
//div[contains(@class, 'ns-view-toolbar-button-spam')]

10. Кнопка "Пометить прочитанным"
//div[contains(@class, 'ns-view-toolbar-button-mark-as-read')]

11. Кнопка "Переместить в другую директорию"
//div[contains(@class, 'ns-view-toolbar-button-folders-actions')]

12. Кнопка "Закрепить письмо"
//div[contains(@class, 'ns-view-toolbar-button-pin')]

13. Селектор для поиска уникального письма
//span[@title='Как читать почту с мобильного']/ancestor::a

На странице яндекс диска
1. Кнопка загрузить файлы
//input[@type='file']

2. Селектор для уникального файла на диске
//input[@class='input__control']

3. Кнопка скачать файл
//button[@id='nb-209']

4. Кнопка удалить файл
//button[@id='nb-210']

5. Кнопка в корзину
//a[contains(@href, '/client/trash')]

6. Кнопка восстановить файл
//button[@id='nb-463']