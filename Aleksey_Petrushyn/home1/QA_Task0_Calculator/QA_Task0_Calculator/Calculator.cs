﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QA_Task0_Calculator
{
	public class Calculator : ICalculator
	{
		public int GetSum(int a, int b)
		{
			return (a + b);
		}

		public int GetSubtraction(int a, int b)
		{
			return (a - b);
		}

		public int GetMultiplication(int a, int b)
		{
			return (a * b);
		}

		public double GetDivision(int a, int b)
		{
			if (b == 0)
				throw new ArgumentException("Divide by zero is here!");
			return (a / b);
		}
	}
}
